COMMANDS = ["f", "b", "l", "r"]
DIRECTIONS = ["N", "E", "S", "W"]
# When projecting the movements on a 2d grid with (0, 0) at the top left corner,
# going up (north) is achieved by reducing the current Y index on the grid, and
# increasing it to go down (south)
MOVEMENTS = {"N": (0, -1), "E": (1, 0), "S": (0, 1), "W": (-1, 0)}