import pytest

from src.models.rover import Rover 
from src.models.terrain import Terrain
from exceptions.rover_exceptions import RoverCommandError, RoverError
from exceptions.terrain_exceptions import TerrainError


def test_bad_rover():
    with pytest.raises(RoverError):
        terrain = Terrain(terrain_x_y=(3, 3), obstacles=[])
        rover = Rover(terrain, direction="B", x=0, y=0)


def test_bad_terrain():
    with pytest.raises(TerrainError):
        terrain = Terrain(terrain_x_y=(0, 0), obstacles=[])
    


def test_bad_command():
    with pytest.raises(RoverCommandError):
        commands = ["f", "j", "f", "r", "f", "l"]
        terrain = Terrain(terrain_x_y=(3, 3), obstacles=[])
        rover = Rover(terrain, direction="N", x=0, y=0)
        rover.execute(commands=commands)


def test_movement():
    commands = ["f", "l", "f", "r", "f", "l"]
    terrain = Terrain(terrain_x_y=(3, 3), obstacles=[])
    rover = Rover(terrain, direction="N", x=0, y=0)
    rover.execute(commands=commands)
    assert rover.position == (2, 1)
