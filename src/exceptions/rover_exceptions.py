class RoverError(Exception):
    def __init__(self, msg: str):
        self.message = msg
        super().__init__(msg)


class RoverCommandError(RoverError):
    def __init__(self, command: str,):
        self.message = f"Command \"{command}\" is not supported."
        super().__init__(self.message)


class RoverDirectionError(RoverError):
    def __init__(self, directions: list):
        self.message = f"Invalid direction! Please provide a valid direction --> {
            directions}"
        super().__init__(self.message)

class ObstacleError(RoverError):
    def __init__(self, obstacle, position):
        self.message = f"The rover found an obstacle at {obstacle} and stopped at {position}"
        super().__init__(self.message)

