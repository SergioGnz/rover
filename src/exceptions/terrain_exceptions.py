
class TerrainError(Exception):
    def __init__(self):
        self.message = "Invalid terrain! Please provide a terrain with X and Y bigger than 0"
        super().__init__(self.message)