from constants import DIRECTIONS, COMMANDS
from exceptions.rover_exceptions import RoverCommandError, RoverDirectionError
from exceptions.terrain_exceptions import TerrainError


def sanitize_direction(direction: str) -> str:
    sanitizedDirection = direction.upper()
    if (sanitizedDirection not in DIRECTIONS):
        raise RoverDirectionError(directions=DIRECTIONS)
    return sanitizedDirection


def sanitize_commands(commands: list[str]) -> list[str]:
    sanitizedCommands = [c.lower() for c in commands]
    for command in sanitizedCommands:
        if command not in COMMANDS:
            raise RoverCommandError(command=command)
    return sanitizedCommands


def validate_terrain(terrain: tuple[int, int]) -> tuple[int, int]:
    if not len(terrain) == 2 or any((n <= 0 for n in terrain)):
        raise TerrainError()
    return terrain
