from models.rover import Rover
from models.terrain import Terrain
from exceptions.rover_exceptions import RoverError, ObstacleError
from exceptions.terrain_exceptions import TerrainError


def start():
    """
    Retrive user data by input and sanitize it to instance a 
    Terrain and a Rover with commands to execute
    """

    print("Input the Terrain X and Y size as x:y")
    inputTerrain = input()
    terrain_x_y = tuple(int(n) for n in inputTerrain.split(":"))

    print("Input any obstacles coordinates as x:y,x:y ... or leave empty")
    obstaclesPos = input()
    if obstaclesPos:
        obstacles = [tuple(int(n) for n in s.split(":"))
                     for s in obstaclesPos.split(",")]
    else:
        obstacles = []
    print("Input the Rover X and Y coordinates as x:y")
    inputPos = input()
    position = tuple(int(n) for n in inputPos.split(":"))

    print("Input the Rover direction (N, E, S, W):")
    facing = input()

    try:
        terrain = Terrain(terrain_x_y, obstacles)
        rover = Rover(terrain, direction=facing, x=position[0], y=position[1])

        print("Enter a command sequence as a string --> ffflflfrbbbrrffl...")
        inputComms = input()
        commands = [s for s in inputComms]
        rover.execute(commands)
        visualize_rover_position(rover, terrain)
    except ObstacleError as oe:
        print(oe)
        visualize_rover_position(rover, terrain)
    except RoverError as re:
        print(re)
    except TerrainError as te:
        print(te)
    except Exception as e:
        print(e)


def visualize_rover_position(rover: Rover, terrain: Terrain) -> None:
    """
    Just a basic function to paint the ending Rover postion
    :param rover: The current Rover being used
    :param terrain: The terrain given to the current Rover
    """

    print(f"The Rover ended at {rover.position}")
    result:str = ""
    for y in range(terrain.size[1]): 
        for x in range(terrain.size[0]):
            if rover.position == (x, y):
                result += " R"
            elif (x, y) in terrain.obstacles:
                result += " X"
            else:
                result += " -"
        result += "\n"
    print(result)

if __name__ == "__main__":
    start()
