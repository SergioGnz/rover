from exceptions.rover_exceptions import RoverError, ObstacleError
from utils import sanitize_commands, sanitize_direction
from constants import DIRECTIONS, MOVEMENTS, COMMANDS
from models.terrain import Terrain


class Rover:
    """
    A simple Rover that works in a 2D terrain.

    :attribute facing: The direction of the Rover(N, E, S, W)
    :attribute position: The current position on the terrain 
    with 0,0 at the top left corner
    """
    facing: str
    position: tuple

    _angle: int
    _turn_angle: int = 90
    _terrain: Terrain

    def __init__(self, terrain: Terrain, direction: str = "N", x: int = 0, y: int = 0):
        self._terrain = terrain
        self.facing = sanitize_direction(direction)
        self._angle = self._turn_angle * DIRECTIONS.index(self.facing)
        if not self._can_move_to(position=(x, y)):
            raise RoverError("The initial position is blocked")
        self.position = (x, y)

    def execute(self, commands: list[str]):
        """
        Used to move the Rover. Raises an exception on bad commands
        or if an obstacle is in the way.

        :param commands: User given commands, sanitized after
        """
        try:
            for command in sanitize_commands(commands):
                if command in ["f", "b"]:
                    self._move(command)
                else:
                    self._rotate(command)
        except AttributeError:
            print(
                f"Invalid commands! Use any of this commands --> {COMMANDS}"
            )

    def _move(self, command: str):
        """
        Checks if the Rover can move to the next position, 
        and wraps such position around the terrain
        """
        movement = MOVEMENTS[self.facing]

        next_x: int = self.position[0] + movement[0] \
            if command == "f" else self.position[0] - movement[0]
        next_y: int = self.position[1] + movement[1] \
            if command == "f" else self.position[1] - movement[1]

        # Wrap positions around the grid
        next_x %= self._terrain.size[0]
        next_y %= self._terrain.size[1]

        nextPosition = next_x, next_y
        if self._can_move_to(nextPosition):
            self.position = nextPosition
        else:
            raise ObstacleError(nextPosition, self.position)

    def _rotate(self, command: str):
        """
        Rotates the Rover by the given command and set it's facing property
        """
        self._angle += self._turn_angle if command == "r" else -self._turn_angle
        self._angle %= 360  # normalize angle in the 0-360 range
        direction_index = int(self._angle/self._turn_angle) % len(DIRECTIONS)
        self.facing = DIRECTIONS[direction_index]

    def _can_move_to(self, position: tuple) -> bool:
        return position not in self._terrain.obstacles
