from utils import validate_terrain

class Terrain():
    """
    A terrain represented in a 2D grid.

    :param terrain_x_y: Width and height of the terrain as (x, y)
    :param obstacles: A list of the position of any obstacles in the terrain
    """
    size: tuple[int, int]
    obstacles: list[tuple]

    def __init__(self, terrain_x_y:tuple[int, int], obstacles: list[tuple]):
        self.size = validate_terrain(terrain_x_y)
        self.obstacles = obstacles
