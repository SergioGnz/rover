# Rover

A basic project for a Rover that works on a 2D world.


## Set up


Install dependencies locally:
```
pip install -r requirements.txt  
```

Run the tests:
```
pytest
```
If pytest doesn't find some modules, try using this command:
```
export PYTHONPATH=$PYTHONPATH:$(pwd)

```

Run the app:

```
python main.py
```

or:
```
python3 main.py
```